<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/home", name="home")
     */
    public function index()
    {

        $_SESSION['random'] = rand(0,100);
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'randomNumber' => $_SESSION['random']
        ]);
    }
}
